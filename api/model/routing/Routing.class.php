<?php
// Custom DAO (to edit / consult before creating another routing method !)
include dirname(__DIR__).'/dao/EntityCRUD.class.php';
/* THINK to change DBNAME on the method, depending of your database / the one you want */

// SubRoute (for a clearer code) => 'group of method' :
    include 'Ad.class.php'; 


// For DEV ONLY, this class extends 'TestRoute'
    include 'TestRoute.class.php';
class Route extends TestRoute{
    
    /**
     * Used by the Route Controller
     * Pass the route and optionnal value and call proper method as the instanciation of the object
     */

    public $entity;
    public $result;
    public $data;
    public $subclass;

    public function __construct($arg, $val){
        $this->result = $this->$arg($val);
    }

    /**
     * @Route("/ads/{value?}", methods={'GET'}, name="ads")
     */
    public function ads($value=''){
        $this->entity = new MyEntity('leptitcoin', 'ad');
        $this->subclass = new Ad();
        return $this->subclass->ads($this->entity);
    }

    /**
     * @Route("/adCreation/", methods={'POST'}, name="adCreation)
     */
    public function adCreation($value){
        $this->entity = new MyEntity('leptitcoin', 'ad');
        $this->subclass = new Ad();
        return $this->subclass->adCreation($this->entity,$value);
    }  

    /**
     * @Route='/detailProd, name="detailProd"
     */

    public function detailProd($arg)
    {
        $this->subclass = new Ad();
        return $this->subclass->detailProd($arg);
    }
    
    /**
     * @Route("/registerCustomer/{val?}", methods={'GET'}, name="test")
     */
    public function registercustomer($val) {
        echo $val = 'coucou';
        $this->entity = new MyEntity('leptitcoin', 'category');
        $data = $this->entity->get("*");
        $json = json_encode($data);
        return $json;
    }
}

