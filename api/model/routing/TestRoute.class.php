<?php 
/**
 * Class that stock all method used for Dev environnement / testing purposes
 */

// Maybe should we use namespace here ?
class TestRoute{

    public function test($str=null)
    {
        $customMsg = isset($str) && $str !== 'test' ? ' By the way, we find this : \''.$str.'\'... maybe you forgot it as amazed you were !':' Oh ! As i can see, you didn\'t send us a present, or something of that kind ! I\'m a bit disappointed... even a STRING was good enough for US... maybe next time !';
        return 'Welcome to \'le Petit Coin API\' ! As you here, just stay calm, breath and relax while thinking of all amazing stuff you can do, know that you connected to thoussand of possibilities ! :) '.PHP_EOL.$customMsg;
    }

    public function hello($world='World')
    {
        $this->name = $world;
        return 'Hello ' . $this->name.' !';
    }

    /**
     * @Route("/json/{size}", methods={'GET'}, name="test")
     */
    public function json($size){
        $this->json=[];
        for($i=0;$i<$size;$i++){
            $this->json['prop'.$i]='value'.$i;
        }
        return json_encode($this->json);
    }

}
?>