<?php 

    class Ad{

        public function ads($entity){
            $data = $entity->get("*", "", "fetchAll");
            $message = $entity->message;
            $dataJson = ['data' => $data, 'message' => $message];
            $json = json_encode($dataJson);
            return $json;
        }

        public function adCreation($entity,$value){
            //session_start();

            $this->customer=$_SESSION['id'];
            $this->category = $value['category'];
            $this->title = $value['title'];
            $this->description = $value['text'];
            $this->price = $value['price'];
            $this->zip_code = $value['zip_code'];
            $this->date = date("Y-m-d");

            return $entity->post("id_customer_id,id_category_id,title,date,description,zip_code,price","'1','$this->category','$this->title','$this->date','$this->description','$this->zip_code','$this->price'");
        }  

        public function detailProd($arg)
        {
            $this->ad = [
                "id" => 0,
                "title" => "Truelle",
                "date" => "2019-10-10",
                "description" => "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Rem, voluptas. Minus ea at quo maxime qui, laudantium veniam magni commodi doloremque, optio deleniti aut. Omnis quas quam dolor nostrum totam.",
                "id_customer" => 0,
                "id_category" => 0,
                "zip_code" => 34400,
                "views" => 15,
                "bookmarked" => 4,
                "price" => "25 euros",
            ];
            $this->customer = [
                "id" => 0,
                "id_account" => 0,
                "name" => "BONNOT",
                "firstName" => "Jean",
                "favorite_place" => "Mtpl",
                "bio" => "blablabla",
                "profil_picture" => "../public/vendeur.jpeg",
                "rank" => "vendeur vérifié",
                "identity_document" => "string",
                "phone" => "01.02.03.04.05",
                "token_google" => null,
                "token_facebook" => null
            ];

            $this->images = [
                ["id" => 0,"url" => 'https://place-hold.it/200x150',"id_ad" => 0],
                ["id" => 0,"url" => 'https://place-hold.it/200x150',"id_ad" => 0],
                ["id" => 0,"url" => 'https://place-hold.it/200x150',"id_ad" => 0]
            ];

            $this->category = [
                "name" => "Bricolage",
            ];

            $this->note = [
                "id" => 0,
                "note" => 5,
                "id_transaction" => 0
            ];

            $this->account = [
                "id" => 0,
                "email" => "blabal@bla.com",
                "pseudo" => "JeanMichMuche",
                "password" => "perlimpimpin",
                "role" => "string"
            ];

            $this->tab = [
                "ad" => $this->ad,
                "customer" => $this->customer,
                "images" => $this->images,
                "category" => $this->category,
                "note" => $this->note,
                "account" => $this->account
            ];

            return json_encode($this->tab);
        }
    }
?>