<?php
/**
 * DAO / API
 * But : 
 * - Délocaliser le traitement de la BDD sur une API 
 * - Plusieurs plateformes pourront se connecter et profiter de ces données 
 * Ex : Admin ; Back-office ; Customers
 */

/**
 * Connexion à la BDD, via instanciation d'un objet PDO
 */

class BDD {

    private $log;
    private $errorlog;
    private $credentials;
    
    public $bdd;
    public $host;
    public $dbName;
    
    public function __construct($dbname, $host = 'localhost'){
        //$rootFolder=dirname(dirname(dirname(__DIR__)));
        $realPath = realpath('/var/www/html/LePtiCoinn/lepticoin_front/app/config/config.ini');
        
        $this->log=[];
        $this->errorlog=[];

        // Récupération des crédentials
        $this->credentials = parse_ini_file($realPath, true);
        
        $this->dbName=$dbname;
        $this->host=$host;

        if($this->setConnexion()){
            $this->printLog('Connected successfuly');
        }else{
            echo 'Connection failed ; Contact your Admin Sys for more informations';
        }
    }

    /**
     * Connexion à la BDD et gestion des erreurs 
     */
    public function setConnexion(){
    
        $bddUser = $this->credentials['DATABASE']['db_user'];
        $bddPass = $this->credentials['DATABASE']['db_password'];

        try{
            $this->bdd = new PDO("mysql:host=".$this->host.';'.'dbname='.$this->dbName,$bddUser,$bddPass);
            $this->bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return true;
        }
        catch(PDOException $e){
            $this->printLog($e->getMessage(),'error');
            return false;
        }
    }

    public function unsetConnexion(){
        $this->bdd=null;
        $this->printLog('Connexion to databse killed ;');
    }
    public function printLog($message,$destination=''){
        $where=$destination.'log';
        $this->$where[]=date('d/m/Y h:i:s').' : '.$message;
    }

    public function execQuery($sql){
        try{
            $this->bdd->exec($sql);
            $this->printLog($sql.' => executed sucessfuly');
        }
        catch(PDOException $e){
            $this->printLog($e->getMessage(),'error');
        }
        $this->unsetConnexion();
    }

    /** DEV OPTIONS 
     * (To Bypass the private attribute of our PDO object) 
     */

    public function callMethod($method,$val){
        try{
            $this->printLog('Call method : '.$method.' on PDO with the following statement : `'.$val.'`');
            return $this->bdd->$method($val);
        }catch(PDOException $e){
            $this->printLog($e->getMessage(),'error');
        }

        $this->unsetConnexion();
        
    }

    public function getAttribute($attr){
        return $this->bdd->$attr;
    }
    public function setAttribute(){

    }
    
    public function __toString(){
        $result = '<h1>Log : </h1>';
        foreach($this->log as $msg){
            $result .= '<pre>'.$msg.'</pre>';
        }

        $result .= '<h1>Error Log : </h1>';
        foreach($this->errorlog as $error){

            $result .= '<pre>'.$error.'</pre>';
        }
        return $result;
    }

}

/* 
$bddTest=new BDD('leptitcoin');
$table=$bddTest->dbName.'.'.'ad'; */
    

/* 
$request = "INSERT INTO $table (`id_customer_id`, `id_category_id`, `title`, `date`, `description`, `zip_code`, `views`, `bookmarked`, `price`) VALUES (9,1,'Jouet boulanger', NOW(),'Jouet pour enfant de 13 à 24 mois, à venir cherchez sur place !\nMerci de me contacter via le site et non mon numéro affichez au dessous de ma bio :)',34080,0,0,'14,35')";
$bddTest->execQuery($request); 
*/

//echo $bddTest;
/**
 * Regroupement de méthodes pour l'accès et la manipulation de la BDD (CRUD)
 */


/**
 * Gestion des routes ;
 * Traîtement des données selon route demandées (dynamiquement)
 */