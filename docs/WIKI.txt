MVC 

Comme vous pouvez le voir, l'application est basée sur l'architecture MVC.
Sans entrez dans les détails, la structure des différents dossier (Bootstraping) se base sur les recommandations du framework 'Zend' (http://framework.zend.com/manual/1.12/en/project-structure.project.html) et plus généralement, le post suivant sur StackOverflow (https://stackoverflow.com/questions/7959673/directory-structure-for-mvc) ainsi que ce projet Git pour la structure d'une API REST (https://github.com/thevatsalshah/rest-api-structure-corephp)

Ce que vous devez impérativement savoir : 
- Le dossier public contient : 
    - le point d'entrez de l'application (index.php)
    - le fichier de configuration pour serveur apache (.htaccess)
    - Fichier CSS, JS, IMG, Fonts (nom est assez explicite je pense)

- Dans le dossier app vous trouverez : 
    - Les dossiers model/, view/ et controller/ (principaux fichier de l'archi MVC)
    - Un dossier lib qui contiendras l'ensemble des fonctions partager sur les fichiers de ce dossier

    - Un dossier config à éditer pour y spécifier vos 'credentials' pour la bdd (pour le moment)

- Le dossier data comprend en gros les 'données', soit la BDD au format 'sql' et des dossiers selon les besoins de l'application (à définir)

- Le dossier lib (à la racine) est plus générale, car il comprend les grosses librairies (il permet de différencier les librairies 'générique' de celles tailler ou 'tailorer' pour les besoins du projet) et contient : 
    - vendors qui sont simplement les librairies tierces (3rd-party)

- Pour finir, le dossier doc qui contiendras les notes de détail de l'application, de configuration et prise en main de celle-ci !


Framework Perso 

Ressources : 
    - Guidelines : https://medium.com/@ivorobioff/5-easy-steps-to-build-your-own-php-framework-cb4ba72dc5a6
    - Projet complet en plusieurs parties : https://medium.com/shecodeafrica/building-your-own-custom-php-framework-part-1-1d24223bab18 (à finir)
    - Constuire une API REST : 
    - Structure MVC d'une API REST (schéma et code) : https://codereview.stackexchange.com/questions/164846/mvc-structured-rest-api-in-php

Organisation - Project Management : 

Trello : 
    - Blog article for website project : https://www.flowji.com/using-trello-to-manage-your-website-project/
    - Template Web Developpement (FullStack) : https://trello.com/b/eSTeGdwr/template-web-development
    - Another template, more client-side oriented (client = customer) : https://trello.com/b/ttptYLbm/client-template-web-development-project

URL / URI / URN :

    Definition des trois termes

    Comment le parser fonctionne en GROS

    Manipulation de string, la fonction explode

    Filtrer les valeur, et vérification supplémentaire :
        - Map ou foreach pour un petit htmlspecialchars si besoin (facultatif)
        - Array_filter avec le callback souhaité pour éviter les artefact fournis par l'explode

    Réindéxation d'un array avec la fonction array_values, qui, retourne simplement les valeurs contenue dans le tableau !