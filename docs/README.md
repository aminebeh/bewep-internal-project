# LePtiCoin Front

Pre-requesites / Installation

    For this project, you basically need :
        - php (>=7.2)
        - mariadb & mysql
        - phpmyadmin (recommanded)
        - composer (not necessarly yet, but that may be needed in a future version)
        - apache server


    1. To get started, you first need to import our database ! 
    Find it at /data/leptitcoin xx.xx.sql
    You may find multiple database, it's okay, your not mad (yet) ! 
    Just take the one with the highest number next to it (i.e. : 1.2);
    Then import it the way, that better suit your needs / which you're the more comfortable with (we don't judge you, it's okay to use a graphic interface :) )

    First step finish ! Don't be foolish, you're not done yet (not at all..)

    2. Go edit the 'config.ini' (from root, go to /app/config/config.ini)
    Those two lines are especially :
        - db_user
        - db_password
    Set them, accordingly to your credentials (the one required to access your newly imported database)

    (PLUS) set the follow names accordingly to our naming convention : 
        - db_name = 'leptitcoin' (when importing the databse)
        - $realPath = 'path/to/this/app/folder'.'/app/config/config.ini' in 'api/model/dao/BDD.class.php';
        - apache_root = 'root/of/your/apache/server' i.e. for linux :'/var/www/html' in (/app/config/config.ini)
        - apache_host = 'domain/name' i.e. for mampp 'localhost:8888' in (/app/config/config.ini)

    3. FIND a more 'generic' way for the apache_root & apache_host (the equivalent to the $_SERVER['DOCUMENT_ROOT']);
    4. Same goes for absolute path declared here and there... (not finish yet!)