<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitce1615b5dc06cc45bd3d06156dd89a3d
{
    public static $prefixLengthsPsr4 = array (
        'A' => 
        array (
            'App\\Controller\\' => 15,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'App\\Controller\\' => 
        array (
            0 => __DIR__ . '/../../..' . '/app/controller',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitce1615b5dc06cc45bd3d06156dd89a3d::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitce1615b5dc06cc45bd3d06156dd89a3d::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
