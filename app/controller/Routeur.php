<?php 
namespace App\Controller;
/**
 * Class for the Routing of our main App
 * Bind some route (localhost/)
 */
Class Routeur{
    
    public function test(){
        echo 'Hello World from autoloaded !';
    }
}

/**
 * Removes trailing forward slashes from the right of the route.
 * @param route (string)
 */