<?php 

    class TestRoute{
        
        public $path;
        protected $param;
        protected $option;
        protected $root;
        protected $title;
        public function __construct($method,$param=null,$option=null){
            $this->path = $method;
            $this->param = $param;
            $this->option = $option;
            $this->root = dirname(__DIR__);
        }

        public function findTitle(){
            $title=[
                    "detail"=>"Details Product",
                    "ad"=>"Ads",
                    "adCreation"=>"Ad Creation",
                    "login"=>"Login",
                    "register"=>"Register",
                    "personalBoard"=>"Personnal Board",
                    "raw"=>"Portal"
            ];
            $this->title = $title[$this->path];
            return $this->title;
        }

        public function renderRoute(){
            $method = $this->path;
            if(isset($this->param)){
                if($this->param !== null)
                    $this->$method($this->param);
            }else{
                $this->$method();
            }
        }
        /**
         * @Route("/detail/{id.ad}", methods={'GET','HEAD'}, name="Details Product")
         */
        public function detail($id=null){
            include $this->root.'/view/detailProduit/detail.php';
        }
        
        /**
         * @Route("/ad/{?filter}", methods={'HEAD','GET','POST'}, name="Ads")
         */
        public function ad($filter=null){
            include $this->root.'/view/ad/ad.php';
        }

        /**
         * @Route("/adCreation/{data}", methods={'HEAD','POST'}, name="Ad Creation")
         */
        public function adCreation($data=null){
            include $this->root.'/view/adCreation/adCreation.php';
        }

        /**
         * @Route("/login/{credentials}", methods={'POST'}, name="Login")
         */
        public function login($credentials=null){
            include $this->root.'/view/login/login.php';
        }

        /**
         * @Route("/register/{infos}", methods={'POST'}, name="Register")
         */
        public function register($infos=null){
            include $this->root.'/view/register/register1.php';
        }

                /**
                     * /!\ FOR TESTING PURPOSE ONLY !
                     *      SAME ROUTE AS ABOVE 
                     */
                public function register2($infos=null){
                    include $this->root.'/view/register/register2.php';
                }

                public function register3($infos=null){
                    include $this->root.'/view/register/register3.php';
                }

        /**
         * @Route("/personalBoard/{account_type}", methods={'POST'}, name="Personnal Board")
         */
        public function personalBoard($account_type=null){
            if($account_type == 'admin'){
                include $this->root.'/view/personalBoard/admin/board.php';
            }else if($account_type == 'customer'){
                include $this->root.'/view/personalBoard/customer/board.php';
            }
        }
                /**
                 * /!\ FOR TESTING PURPOSE ONLY !
                 *      SAME ROUTE AS ABOVE 
                 */
                public function admin($account_type=null){
                    include $this->root.'/view/personalBoard/admin/board.php';
                }
                public function customer($account_type=null){
                    include $this->root.'/view/personalBoard/customer/board.php';
                }

        /**
         * @Route("/", methods={'GET','HEAD'}, name="raw")
         */
        public function raw(){
            include $this->root.'/view/root/raw.php';
        }

        /**
         * ERROR Routing : 
         * @Route("/?error={code}", methods={'GET', name="error custom")
         */
        public function error(){
            include $this->root.'/view/error/404.html';
        }
    }
?>