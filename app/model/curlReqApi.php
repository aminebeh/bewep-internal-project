<?php 
/* $uriParsed = 'annonces';
$title=strtoupper(substr($uriParsed,0,1)).substr($uriParsed,1); */

   //INIT cURL
   $curl = curl_init();

   $url=__DIR__.'../../testApi/api.php';

   /**
    * Linux : /var/www/html/
    * Windows (Xampp) : C:\WebDeveloppement\xampp\htdocs\
    * Mac (Mamp) : /Applications/MAMP/htdocs ; localhost:8888
    */
   
   $url=str_replace('/var/www/html/','localhost',$url);
   //echo $url; 
   
   //Configuration
   $curl_options=array(
       CURLOPT_URL=>$url,
       CURLOPT_HEADER=>false,
       CURLOPT_RETURNTRANSFER=>true
   );
 
   curl_setopt_array($curl,$curl_options);
 
    // Execution
   $myjson=curl_exec($curl); 
 
    // END (Close)
    curl_close($curl);
 
    //OUTPUT : 
    //var_dump($myjson);
    $jsonDecoded=json_decode($myjson,true);
    //var_dump($jsonDecoded);
    return $jsonDecoded;
?>