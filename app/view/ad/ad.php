<?php
//$uriParsed = 'annonces';
//$title=strtoupper(substr($uriParsed,0,1)).substr($uriParsed,1);
   // INIT cURL
  $curl = curl_init();
  $url=__DIR__.'/back/api.php?action=ads'; //appelle la methode json et envoie la valeur 10
  $url=str_replace($replaceRoot,$replaceHost,$url);
  //echo $url; 

   // Configuration
  $curl_options=array(
      CURLOPT_URL=>$url,
      CURLOPT_HEADER=>false,
      CURLOPT_RETURNTRANSFER=>true
  );

  curl_setopt_array($curl,$curl_options);

   // Execution
  $myjson=curl_exec($curl); 

   // END (Close)
   curl_close($curl);

   //OUTPUT : 
   $jsonDecoded=json_decode($myjson,true);
  
   /* echo '<pre>';
   var_dump($jsonDecoded);
    echo '</pre>';
 */
?>
<div class="container-fluid mt-5">
    <!--Filtre-->
    <div class="row">
        <div class="col-2 text-center">
            <div class="dropdown">
                <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Catégories
                </a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink"> 
                    <div class="dropdown-divider"></div>   
                        <a class="dropdown-item" href="#">MAISON</a>
                    <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Appartement</a>
                        <a class="dropdown-item" href="#">Maison</a>
                    <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">IMMOBILIER</a>
                    <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Meuble</a>
                        <a class="dropdown-item" href="#">électroménager</a>
                    <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">VEHICULES</a>
                    <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">motos</a>
                        <a class="dropdown-item" href="#">velos</a>
                    <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">MULTIMEDIA</a>
                    <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">jeux video</a>
                    <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">LOISIRS</a>
                    <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">balon</a>
                    <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">MODE</a>
                    <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">String</a>
                    </div>
                </div>
            </div>
    <div class="col-6 input-group ">
        <input class="form-control "type="text" placeholder="Que recherchez vous ?">
    </div>
        <div class="col-4 text-center"><input class="form-control" type="text" placeholder="Lieu">
    </div>
    </div>

<!-- RESULT OF THE RESEARCH -->

    <div class=mb-5> 
        <div class="row">
        <div class="col-10 offset-1 text-center">Résultat trouvé : <?php echo $jsonDecoded['data'][0]['views'] ?> </div>
        </div>
    </div>

<!-- Carde -->
<?php 
foreach($jsonDecoded['data'] as $ad){
?>

    <hr>
    <div class="row">
        <div class="col-4 offset-1">
            <img class="img-thumbnail"src="https://placekitten.com/640/360" alt="chat">
        </div>

        <div class="col-5"> 
            <ul>
                <li>Nom:&nbsp<?php echo $ad['title'];?></li>
                <li>Prix:&nbsp<?php echo $ad['price'];?></li>
                <li>Catégories:&nbsp<?php echo $ad['id_category_id'];?></li>
                <li>Localisation:&nbsp<?php echo $ad['zip_code'];?></li>
            </ul>
        </div>
        <div class="col-1">
            <button type="button" class="btn btn-outline-success">Favoris</button>
            <p>Date:<?php echo $ad['date'];?></p>
        </div>
    </div>
    

<?php // endif 
}
?>
<!-- Pagination -->
    <div class=mb-5>
        <ul class="pagination justify-content-center">
            <li class="page-item">
            <a class="page-link" href="#" aria-label="Previous">
                <span aria-hidden="true">&laquo;</span>
                <span class="sr-only">Précédent</span>
            </a>
            </li>
            <li class="page-item">
                <a class="page-link" href="#">1</a>
            </li>
            <li class="page-item">
                <a class="page-link" href="#">2</a>
            </li>
            <li class="page-item">
                <a class="page-link" href="#">3</a>
            </li>
            <li class="page-item">
                <a class="page-link" href="#" aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                    <span class="sr-only">Suivant</span>
                </a>
            </li>
        </ul> 
    </div>
</div>