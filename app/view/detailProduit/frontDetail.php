<?php


// INIT cURL
$curl = curl_init();
$url = __DIR__ . '/api/api1.php?action=detailProd';
$url = str_replace($replaceRoot, $replaceHost, $url);

// Configuration
$curl_options = array(
    CURLOPT_URL => $url,
    CURLOPT_HEADER => false,
    CURLOPT_RETURNTRANSFER => true
);

curl_setopt_array($curl, $curl_options);

// Execution
$myjson = curl_exec($curl);

// END (Close)
curl_close($curl);

//OUTPUT : 

$detail = json_decode($myjson, true);
//var_dump($test);
