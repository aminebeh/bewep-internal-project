<?php
include('frontDetail.php');
?>
<head>
    <title> <?php echo $detail['ad']['title']; ?></title>
</head>

<body>
    <!-- FIL D ARIANE -->
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Accueil</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php echo $detail['category']['name']; ?></li>
            </ol>
        </nav>

        <div class="row">
            <div class="col-7">
                <!-- IMAGES -->
                <!-- Shell of our Carousel -->
                <div id="detailCarousel" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <?php
                            $countImage = 0;
                            foreach ($detail['images'] as $image) {
                        ?>
                            <li data-target="#detailCarousel" data-slide-to="<?php echo $countImage++; ?>" class="puce-carousel active"></li>
                        <?php 
                        }; // end -- foreach
                        ?>
                    </ol>
                    <div class="carousel-inner">
                        <!-- Images in it -->
                        <?php
                        $active = true;
                        foreach ($detail['images'] as $imageArray) { ?>
                            <div class="carousel-item <?php if ($active) {
                                                                echo "active";
                                                                $active = false;
                                                            } ?>">
                                <img class="d-block w-100" src="<?php echo $imageArray['url']; ?>" alt="Ad slide">
                            </div>
                        <?php
                        }; // -- end foreach
                        ?>
                    </div>
                    <a class="carousel-control-prev" href="#detailCarousel" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#detailCarousel" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
                <!-- // FIN IMAGES  -->

                <!-- ZONE INFOS ANNONCE: -->
                <!-- INFO TITRE -->
                <div class="row mb-3">
                    <div class="col-6">
                        <h5><?php
                            echo $detail['ad']['title'];
                            ?></h5>
                    </div>

                    <!-- INFO PRIX -->
                    <div class="col-6">
                        <h5><?php
                            echo $detail['ad']['price'];
                            ?></h5>
                    </div>
                </div>

                <!-- INFO DATE -->
                <div class="row mb-3">
                    <div class="col-6">
                        <h5 class="card-title">
                            <?php
                            echo $detail['ad']['date'];
                            ?></h5>
                    </div>

                    <!-- INFO PARTAGE ANNONCE -->
                    <div class="col-6">
                        <a href="<?php echo $_SERVER['REQUEST_URI']; ?>" class=" btn btn-primary">Share</a>
                    </div>
                </div>

                <!-- INFO DESCRIPTION -->
                <div>
                    <p class="mt-5">
                        <?php
                        echo $detail['ad']['description'];
                        ?></p>
                </div>
            </div>

            <!-- ZONE INFO VENDEUR -->
            <div class="col-5">
                <div class="row">
                    <div class="col-5">

                        <!-- PHOTO PROFIL VENDEUR -->
                        <img src="<?php echo $detail['customer']['profil_picture']; ?>" class="card-img-top" alt="vendeur">
                    </div>

                    <div class="col-7">
                        <div class="row mb-4">

                            <!-- INFO PSEUDO VENDEUR ET NOTE -->
                            <a href="<?php echo $_SERVER['REQUEST_URI']; ?>" class="btn btn-light">
                                <?php
                                echo $detail['account']['pseudo']; ?>
                                <span class=" badge badge-warning">
                                    <?php
                                    echo $detail['note']['note']; ?>
                                </span>
                            </a>

                        </div>

                        <!-- INFO CONSULTATION AUTRES ANNONCES DU VENDEUR -->
                        <div class="row">
                            <a href="<?php echo $_SERVER['REQUEST_URI']; ?>" class="btn btn-light">Consulter mes autres annonces</a>
                        </div>

                        <!-- INFO CONTACT MAIL -->

                        <div class="row mt-5">
                            <a id="sellerMail" class="btn btn-light" href="<?php if (!isset($_SESSION['id'])) {
                                                                                echo "../logIn/customerConnexion.php"; //a changer, mettre la page accueil
                                                                            } ?> ">Me contacter par Mail</a>
                            <div style="display:none;" id="showMail">
                                <?php
                                if (isset($_SESSION['id'])) {
                                    echo $detail['account']['email'];
                                }
                                ?>
                            </div>
                        </div>

                        <!-- INFO CONTACT TEL -->
                        <div class="row mt-3">
                            <a id="sellerTel" class="btn btn-light" href="<?php if (!isset($_SESSION['id'])) {
                                                                                echo "../logIn/customerConnexion.php"; /* redirection a modifier */
                                                                            } ?>">Me contacter par Téléphone</a>
                            <div style="display:none;" id="showNb"><?php
                                                                    if (isset($_SESSION['id'])) {
                                                                        echo $detail['customer']['phone'];
                                                                    }
                                                                    ?>
                            </div>
                        </div>
                    </div>

                    <!-- INFO ICONE CHECK VENDEUR VERIFIE -->
                    <div class="row">
                        <img src="../public/usercheck.png" id="sellerIcon" width="30" height="30">
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
    </div>

    <!-- AFFICHAGE ALERT VENDEUR VERIFIE -->
    <script>
        $('document').ready(function() {
            $("#sellerIcon").mouseenter(function() {
                alert("Garantie Vendeur vérifié !")
            });
        });
    </script>

    <!-- SELLER MAIL DISPLAy -->
    <script>
        $('document').ready(function() {
            $("#sellerMail").click(function() {
                $("#showMail").slideToggle("slow");
            });
        });
    </script>

    <!-- SELLER TELEPHONE NUMBER DISPLAy -->
    <script>
        $('document').ready(function() {
            $("#sellerTel").click(function() {
                $("#showNb").slideToggle("slow");
            });
        });
    </script>

    <!-- CAROUSEL ANIMATION -->
    <script>
        $('document').ready(function() {
            $('#detailCarousel').carousel({
                interval: 2000,
                pause: "hover"
            });
        });
    </script>

</body>
