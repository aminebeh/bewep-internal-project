
<div class="container">
    <div class="row">
        <div class="col-7">
            <div class="row">
                <img src="https://place-hold.it/200" class="card-img-top" alt="brouette">
            </div>
            <div class="row mb-3">
                <div class="col-6">
                    <h5>Titre</h5>
                </div>
                <div class="col-6">
                    <h5>Prix</h5>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-6">
                    <h5 class="card-title">Date</h5>
                </div>
                <div class="col-6">
                    <a href="#" class="btn btn-primary">partager</a>
                </div>
            </div>
            <div>
                <p class="mt-5">Lorem ipsum dolor sit amet consectetur adipisicing elit. Repellendus iure quam rem repellat dignissimos vitae. Nisi, officia labore, obcaecati mollitia amet, consequatur exercitationem delectus voluptate quis recusandae ex reiciendis illum?</p>
            </div>
        </div>


        <div class="col-5">
            <div class="row">
                <div class="col-5">
                    <img src="https://place-hold.it/150" class="card-img-top" alt="vendeur">
                </div>
                <div class="col-7">
                    <div class="row mb-4">
                        <h4>Jean Michel <span class="badge badge-warning"> Note</span></h4>
                    </div>
                    <div class="row">
                        <a href="#" class="btn btn-light">Voir toutes mes annonces</a>
                    </div>
                    <div class="row mt-5">
                        <p>me contacter par mail</p>
                    </div>
                    <div class="row mt-3">
                        <p>me contacter par mail</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
