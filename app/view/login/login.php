<?php //include './accessCustomConnexion.php'; ?>
<?php
    if(isset($message)) {
        foreach($message as $key => $value){
            if(count($message[$key]) != 0){
                echo '<div class="alert alert-'.$key.'" role="alert">';
                    echo '<ul>';
                    for($i = 0; $i < count($message[$key]); $i++) {
                        echo '<li>';
                        echo $value[$i];
                        echo '</li>';
                        }
                    echo '</ul>';  
                echo '</div>';
            }
        }
    }
?>
<div class="bg-info">
    <div class="row">
        <div class="col-8 offset-2 mb-5 bg-light mt-5 rounded-lg">
            <div class="row">
                <div class="col-6">
                    <img src="../img/img2.png" width="100%">
                </div>
                <div class="col-6 p-5 border-left">
                    <h2 class="text-center">Se connecter</h2>
                    <div class="col-6 offset-3 my-4 border border-primary"></div>
                    <span class="font-weight-bold text-danger">*</span><small class="text-danger"> Champs requis</small>
                        <form method="post">
                            <div class="form-group">
                                <input type="email" class="form-control form-control-lg rounded-pill" placeholder="Email" name="email">
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control form-control-lg rounded-pill" placeholder="Password" name="email">
                            </div>
                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-primary btn-lg btn-block rounded-pill">Connexion</button>
                            </div>
                        </form>
                        <hr class="mt-4">
                        <div class="text-center mt-4">
                            <a href="forgot-password.html">Mot de passe oublié ?</a>
                        </div>
                        <div class="text-center">
                            <a href="register">Créer un compte</a>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
