
<footer class="page-footer pt-3 bg-secondary text-white">
    <!-- Footer Links -->
    <div class="col-10 offset-1">
        <div class="container-fluid text-center text-md-left">
        <!-- Grid row -->
            <div class="row">
            <!-- Grid column -->
            <div class="col-6">
                <!-- Content -->
                <h5 class="text-uppercase">Footer Content</h5>
                <p>Here you can use rows and columns to organize your footer content.</p>
            </div>
            <!-- Grid column -->
            <hr class="clearfix w-100 d-md-none pb-3">
            <div class="col-6 text-right">
                <!-- Links -->
                <h5 class="text-uppercase">Réalisé par :</h5>
                    <p>Bryan, Astride, Amine, Clement</p>
            </div>
            <!-- Grid column -->
            </div>
            <!-- Grid row -->
        </div>
    <!-- Footer Links -->
    <!-- Copyright -->
        <div class="p-4">
            <div class="footer-copyright text-center">© 2019 Copyright : LePtiCoin
                
            <a href="https://mdbootstrap.com/education/bootstrap/">MDBootstrap.com</a>
            </div>  
        </div>
    <!-- Copyright -->
    </div>
</footer>
